#!/usr/bin/env python3

import configparser, sys, smtplib
from directory_backup import DirectoryBackup
from mysql_dump import MysqlDump
from email.mime.text import MIMEText
from datetime import datetime

def main():
    config = configparser.ConfigParser()
    config_file = sys.argv[1]

    config.read(config_file)
    archiving_stats = {}
    if 'directory' in config:
        target = config['directory']['target']
        dest = config['directory']['destination']

        dir_backup = DirectoryBackup()
        dir_backup.set_target(target)
        dir_backup.set_destination(dest)

        dir_archiving_stats = dir_backup.backup()
        archiving_stats['Directory Stats'] = dir_archiving_stats

    if 'mysql' in config:
        db_host = config['mysql']['host']
        db_user = config['mysql']['user']
        db_pass = config['mysql']['pass']
        dest =  config['mysql']['destination']

        sql_dump = MysqlDump(db_host, db_user, db_pass, dest)
        sql_archiving_stats = sql_dump.run_mysql_dump()
        archiving_stats['MySQL Stats'] = sql_archiving_stats

    email_body = create_email_body(archiving_stats)
    send_completion_email(config['mail'], email_body)

def send_completion_email(mail_config, email_body):
    """
    Sends completion email via MailGun
    """
    msg = MIMEText(email_body)
    msg['Subject'] = "Archiving Job Completed"
    msg['From']    = mail_config['sender']
    msg['To']      = mail_config['recipient']

    s = smtplib.SMTP(mail_config['smtp_host'], 587)

    s.login(mail_config['smtp_user'], mail_config['smtp_password'])
    s.sendmail(msg['From'], msg['To'], msg.as_string())
    s.quit()

def create_email_body(archiving_stats):
    """
    Creates the body of the notification email
    """
    email_body = "Archiving Stats for : " + datetime.today().strftime('%Y-%m-%d') + "\n"
    
    for stats_type, stats in archiving_stats.items():
        email_body = email_body.join("<b>" + stats_type + "</b>")

        for stat, value in stats.items():
            email_body = email_body.join("<b>" + stat + ":</b> " + value)
            email_body = email_body.join("\n")

        email_body = email_body.join("\n")

    return email_body

if __name__ == '__main__':
    main()