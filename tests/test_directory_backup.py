import pytest, tempfile, os, glob, tarfile
from backup.directory_backup import DirectoryBackup

class TestDirectoryBackup():
    def setup_method(self, method):
        self.backup_directory = DirectoryBackup()
        self.test_dir = tempfile.mkdtemp()
       
    def test_fail_when_target_is_not_a_directory(self):
        self.backup_directory.set_target(os.path.basename(__file__))
        with pytest.raises(AssertionError):
            self.backup_directory.backup()

    def test_fail_when_target_is_not_set(self):
        with pytest.raises(AssertionError):
            self.backup_directory.backup()
    
    def test_backup_directory_and_file_should_exist(self, tmp_path):
        target = tmp_path / "target"
        target.mkdir()
        p = target / "hello.txt"
        p.write_text(u'Hello World')

        dest = tmp_path / "destination"
        dest.mkdir()

        self.backup_directory.set_target(target)
        self.backup_directory.set_destination(dest)
        self.backup_directory.backup()

        files = []

        for r, d, f in os.walk(dest):
            for file in f:
                if '.tar.gz' in file:
                    files.append(os.path.join(r, file))

        for f in files:
            tar = tarfile.open(f)
            assert len(tar.getnames()) == 2 # Directory and file

        assert len(list(tmp_path.iterdir())) == 2
        assert len(list(dest.iterdir())) == 1

